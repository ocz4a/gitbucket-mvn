## gitbucket-mvn

Build [gitbucket](https://github.com/gitbucket/gitbucket) with Maven

#### Build

1. Fetch a source release from https://github.com/gitbucket/gitbucket/releases/tag/4.22.0
2. Unpack the source archive
3. Copy src folder to this project
4. Optionally apply patches

    ```
        patch -p0 < patches/foo.patch
    ```


4. Invoke maven build

    ```
        mvn -DskipTests package
    ```

5. Deploy war-file from target to some servlet container
